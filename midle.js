

const serchName = (id, arr) => {
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id == id) {
            return arr[i].nombre;
        }
    } return false;
}


const deletePeople = (id, arr) => {
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id == id) {
            let nombre = arr[i].nombre;
            arr.splice(i, 1);
            return nombre;
        }
    }
}


const modifyPeople = (id, arr, newName) => {
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id == id) {
            console.log("NOMBRE VIEJO " + arr[i].nombre);
            arr[i].nombre = newName;
            console.log("NOMBRE NUEVO " + arr[i].nombre);
        }
    }
}

const booksPeople = (id, arr) => {
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id == id) {
            return arr[i].libros;
        }
    }
}


// BOOL >>> VARIABLE QUE SIRVE PARA :
// EN CASO DE ENCONTRAR EL ID, ARRASTRAR EL FALSE LO QUE QUEDA DE BUSQUEDA 
// EN CASO DE NO ENCONTRAR EL ID SALIR DE LA FUNCION CON UN TRUE

// existID >>> VERIFICA SI EL ID ENVIADO EXISTE EN EL ARR ENVIADO
const existID = (id, arr) => {
    let bool = true;
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id === id) {
            bool = bool && false;
        } else {
            bool = bool && true;
        }
    }
    return bool;
}

const existPeople = (name, surname, id, arr) => {
    //AUX >>> VARIABLE QUE SIRVE PARA:
    //AUX.BOOLEAN = INDICAR SI HAY O NO UN ERROR
    //AUX.TEXT = ENVIAR EL ERROR

    let aux = {
        boolean: false,
        text: ""
    };

    if (existID(id, arr) === true) {
        for (let i = 0; i <= arr.length - 1; i++) {
            if (arr[i].nombre == name && arr[i].apellido == surname) {
                aux.boolean = true;
                aux.text = "El/La autor/a ya existe.";
            }
        }
    } else {
        aux.boolean = true;
        aux.text = "El ID ya esta en uso.";
    }
    return aux;
}


// BUSCA AL AUTOR/A Y RETORNA SU NUMERO EN EL INDEX 
const returPeople = (id, arr) => {
    for (let i = 0; i <= arr.length - 1; i++) {
        if (arr[i].id == id) {
            return i;
        }
    }
    return false;
}

const existBook = (id, data, arr) => {

// auxArr >>> VARIABLE QUE SIRVE PARA ALMACENAR EL INDEX ENCONTRADO EN LA FUNCION
// >>>returPeople<<<

    let aux = {
        boolean: true,
        text: "El/La autor/a no existe."
    };

    id = parseInt(id);
    idBOOk = parseInt(data.id);

    let auxArr = returPeople(id, arr);
    if (auxArr !== false) {
        arr = arr[auxArr].libros;
        if (existID(idBOOk, arr) === true) {
            for (let i = 0; i <= arr.length - 1; i++) {
                if (arr[i].titulo == data.titulo) {
                    aux.boolean = true;
                    aux.text = "El libro ya existe.";
                    return aux;
                }
            }
            arr.push(data);
            aux.boolean = false;
            return aux;
        }
        aux.boolean = true;
        aux.text = "El ID ya esta en uso.";
        return aux;
    }
    return aux;
}

const serchBook = (id, idBOOk, arr) => {
    let aux = false;
    id = parseInt(id);
    idBOOk = parseInt(idBOOk);

    let auxArr = returPeople(id, arr);
    if (auxArr !== false) {
        arr = arr[auxArr].libros;
        for (let i = 0; i <= arr.length - 1; i++) {
            if (arr[i].id === idBOOk) {
                return arr[i];
            }
        }
    }
    return aux;
}

const modifyBook = (id, idBook, data, arr) => {
    let aux = false;
    id = parseInt(id);
    idBook = parseInt(idBook);

    let auxArr = returPeople(id, arr);
    if (auxArr !== false) {
        arr = arr[auxArr].libros;
        for (let i = 0; i <= arr.length - 1; i++) {
            if (arr[i].id === idBook) {
                arr[i] = data;
                return arr[i];
            }
        }
    }
    return aux;
}

const deleteBook = (id, idBook, arr) => {
    let aux = false;
    id = parseInt(id);
    idBook = parseInt(idBook);

    let auxArr = returPeople(id, arr);
    if (auxArr !== false) {
        arr = arr[auxArr].libros;
        for (let i = 0; i <= arr.length - 1; i++) {
            if (arr[i].id === idBook) {
                arr.splice(i, 1);
                return arr[i];
            }
        }
    }
    return aux;
}

module.exports = {
    serchName,        // >>>>>> BUSCA EL NOMBRE DE UN AUTOR/A                                                               
    deletePeople,     // >>>>>> BORRA A UN AUTOR/A                                                     
    modifyPeople,     // >>>>>> MODIFICA A UN AUTOR/A                                                    
    booksPeople,      // >>>>>> MUESTRA LOS LIBROS DE UN AUTOR/A                                                       
    existPeople,      // >>>>>> CREA UN NUEVO AUTOR/A                                                
    existBook,        // >>>>>> CREA UN NUEVO LIBRO DEL AUTOR/A                                                  
    serchBook,        // >>>>>> MUESTRA UN LIBRO EN ESPECIFICO                                                   
    modifyBook,       // >>>>>> MODIFICA LOS DATOS DE UN LIBRO                                                       
    deleteBook        // >>>>>> BORRA UN LIBRO                                                  
};